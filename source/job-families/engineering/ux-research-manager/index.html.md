---
layout: job_family_page
title: "UX Research Manager"
---

## UX Management Roles at GitLab

Managers in the UX Research department at GitLab see the team as their product. While they are credible as researchers and know the details of what UX Researchers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of UX Research commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### UX Research Manager

The User Experience (UX) Research Manager reports to the Director of UX, and UX Researchers report to the UX Research Manager.

#### Responsibilities

* Lead the prioritization of GitLab's research strategy in collaboration with product managers, UX, marketing, and engineering to focus on initiatives that have the most impact.
* Hire a world class team of UX Researchers.
* Oversee UX research activities, ensuring the defined objectives are achieved with the final deliverables and recommendations.
* Socialize actionable research insights using multi-modal communication: presentations, 1:1's, and announcements across the organization.
* Evangelize the benefits of research-driven design across the organization, providing educational content and training when needed.
* Directly manage, coach and grow UX researchers' skill sets and capabilities including key partners such as UX designers and product managers.
* Partner with support and sales to identify, prioritize and address UX-related issues.
* Coordinate research tools and vendors.
* Ensure the maintanence of the UX Research archive.
* Draft quarterly UX Research OKRs
* Delivers input on promotions, function changes, demotions, and firings in consultation with the Director of UX.
* Hold regular 1:1's with all members of their team.
* Create a sense of psychological safety on your team.
* Give clear, timely, and actionable feedback
* Works with all parts of the organization (e.g. Backend, Frontend, Build, etc.) to improve overall UX
* Strong sense of ownership, urgency, and drive
* Excellent written and verbal communication skills, especially experience with executive-level communications
* Ability to make concrete progress in the face of ambiguity and imperfect knowledge

#### Requirements

* A minimum of 3 years managing a group of researchers.
* A thorough understanding of current quantitative and qualitative user-centered design methodologies, modeling, analytics, and what methods to apply to best answer a given objective.
* Quantitative research experience such as A/B testing, SQL, statistical analysis, and data analysis.
* Proficiency with pre-visualization software (e.g. Sketch, Adobe Photoshop, Illustrator).
* Experience defining the high-level strategy (the why) and creating design deliverables (the how) based on research.
* Passion for creating visually pleasing and intuitive user experiences.
* Collaborative team spirit with great communication skills.
* You share our [values](/handbook/values), and work in accordance with those values.

**NOTE** In the compensation calculator below, fill in "Manager" in the `Level` field for this role.

#### Interview Process

- [Screening call](/handbook/hiring/#screening-call) with a recruiter
- Interview with a UX Director (manager)
- Interview with a UX Designer (report)
- Interview with a UX Manager (peer)
- Interview with VP of Engineering
